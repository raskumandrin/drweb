#!/usr/bin/perl -w
use strict;
use POSIX;
use POSIX ":sys_wait_h";
use IO::Socket;
use IO::Handle;

my $pid = fork();
exit() if $pid;
die "Couldn't fork: $! " unless defined($pid);
POSIX::setsid() or die "Can't start a new session $!";

my $server;
my $terminate_signal = 0;
my @allowed_commands;
&HUP;

$SIG{INT} = $SIG{TERM} = \&TERM;
$SIG{HUP} = \&HUP;

my $port = 23000;

$server= new IO::Socket::INET(
    LocalPort => $port,
    TYPE      => SOCK_STREAM,
    Reuse     => 1,
    Listen    => 10,
) or die "Couldn't be a tcp server on port $port: $@";

sub REAPER {
    while ( (my $waitedpid = waitpid(-1,WNOHANG)) > 0 ) { }
    $SIG{CHLD} = \&REAPER;
};

sub TERM {
    $terminate_signal = 1;
    close($server);
};

sub HUP {
    @allowed_commands=();
    my $conf_name = '/etc/simple-telnetd.conf';
    if (-e $conf_name) {
        open(my $fh, '<', $conf_name) or die "Can't open config file $conf_name";
        while (<$fh>){
            chomp;
            push @allowed_commands, $_;
        }
        close $fh;
    }
    else {
        @allowed_commands = qw(pwd ls cd);
    }
};

until($terminate_signal){

    my $client;
    
    while($client = $server->accept()){
        
        $SIG{CHLD} = \&REAPER;

        defined(my $child_pid=fork()) or die "Can't fork new child $!";

        next if $child_pid;

        if($child_pid == 0) {
            close($server);
        }

        $client->autoflush(1);
        print $client 'Enter command: ';

        while (my $command = <$client>) {
            chomp $command;
            close($client) if $command eq 'quit';

            foreach (@allowed_commands) {
                if ($command =~ /^$_/) {
                    print $client `$command`;
                }
            }

        }
        continue {
            print $client 'Enter command: ';
        }
        exit;
    }
    continue {
        close($client);
    }
    
}